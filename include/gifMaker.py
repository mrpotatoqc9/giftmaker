import sys, os
import cv2
import imageio


class GifMaker:
	def __init__(self, videoPath:str, outPath:str, width=640, height=360, maxOutputFileSize=0):
		# gif are 10 fps
		self.__videoPath = videoPath
		self.__maxOutputFileSize = maxOutputFileSize * 1048576
		self.__fileName = os.path.basename(videoPath)
		self.__videoName = os.path.splitext(self.__fileName)[0] # no extension
		self.__outputFileName = os.path.join(outPath, self.__videoName+".gif")
		self.__images = []
		self.__videoDuration = 0
		self.__outputImages = []
		self.__frameToKeepPerSecond = 2
		self.__frameToKeepPerSecond_isSet = False
		self.__gifDuration = 6
		self.__gifDuration_isSet = False
		self.__width = width
		self.__height = height
		self.__progress = 0
		self.__loop = 0
	
	
	def frame_2_keep_per_second_is_set(self):
		return self.__frameToKeepPerSecond_isSet
	
	def gif_duration_is_set(self):
		return self.__gifDuration_isSet
		
	def generateGif(self):
		
		self.__get_video_duration()
		self.__extract_images()
		self.__optimize_output_result()
		
		frame_count = 0
		total_frames = len(self.__outputImages)
		with imageio.get_writer(self.__outputFileName, mode="I", loop=self.__loop) as writer:
			for frame in self.__outputImages:
				writer.append_data(frame)
				frame_count += 1
				currentProgess = round((frame_count / len(self.__outputImages))*33)
				
				if 66 + currentProgess < 100:
					self.__progress = 66 + currentProgess
				else:
					self.__progress = 99
					
		self.__progress = 100
		return self.__progress
		
	def set_gif_duration(self, duration:int):
		if self.__frameToKeepPerSecond_isSet == True:
			sys.stderr.write("frame to keep per second is already set{}".format(os.linesep))
		else:
			self.__gifDuration_isSet = True
			self.__gifDuration = duration
		
	
	def set_frame_2_keep_per_second(self, frameToKeepPerSecond:int):
		if self.__gifDuration_isSet == True:
			sys.stderr.write("gif duration is already set{}".format(os.linesep))
		else:
			self.__frameToKeepPerSecond_isSet = True
			self.__frameToKeepPerSecond = frameToKeepPerSecond
	
	
	def __get_video_duration(self):
		cap = cv2.VideoCapture(self.__videoPath)
		self.__videoDuration = round(cap.get(cv2.CAP_PROP_FPS))
		
	
	def __extract_images(self):
		frame_count = 0
		fps = 30
		# TODO add try catch
		cap = cv2.VideoCapture(self.__videoPath)
		
		while(cap.isOpened()):
			ret, frame = cap.read()
			
			if ret:
				frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
				frame = cv2.resize(frame, (self.__width, self.__height),interpolation = cv2.INTER_NEAREST)
				self.__images.append(frame)
				frame_count += 1
				currentProgess = round(((frame_count/fps)/self.__videoDuration) * 33) # XXX we assume video is 30 fps
				if currentProgess < 33: 
					self.__progress = currentProgess
				else:
					self.__progress = 33
				
			else:
				cap.release()
	
	
	def __get_best_images(self, images:list):
		variations = []
		for img in images:
			variations.append(cv2.Laplacian(img, cv2.CV_64F).var())
		maxVariation = max(variations)
		return images[variations.index(maxVariation)]
		
	
	def __optimize_output_result(self):
		
		videoFps = int(len(self.__images) / self.__videoDuration)
		skipImage = round(videoFps / self.__frameToKeepPerSecond)
		
		if self.__frameToKeepPerSecond_isSet == True:
			pass; # same math as above
		
		elif self.__gifDuration_isSet == True:
			skipImage = round(len(self.__images) / (self.__gifDuration * 10))
		
		# elif
		# maxImages = int(self.maxOutputFileSize / sys.getsizeof(self.images[0])) #TODO max ouput size giff
		
		
		i = 0
		while(i+skipImage < len(self.__images)):
			partImagesArray = self.__images[i:i+skipImage]
			self.__outputImages.append(self.__get_best_images(partImagesArray))
			i+=skipImage+1
			currentProgess = round((i / len(self.__images)) * 33)
			
			if 33 + currentProgess < 66:
					self.__progress = 33 + currentProgess
			else:
				self.__progress = 66
	
	
	def get_progress(self):
		return self.__progress
