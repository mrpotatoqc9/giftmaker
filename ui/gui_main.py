from tkinter import *
from tkinter.ttk import *
from time import strftime

from gui_menubar import build_menu_bar
from gui_grid import BuildGrid

class UI(Tk):
	def __init__(self):
		super().__init__()
		self.title('Gif maker')
		self.geometry('768x576')
		grid = BuildGrid(self, text="Options")
		grid.pack(side="top", fill="both", expand=True)


app = UI()
app.mainloop()
