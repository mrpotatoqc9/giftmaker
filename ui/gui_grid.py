from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *
import tkinter.messagebox
import os, sys
PROJECT_ROOT = os.path.abspath(os.path.join(
                  os.path.dirname(__file__), 
                  os.pardir)
)
sys.path.append(os.path.join(PROJECT_ROOT, "include"))

from gifMaker import GifMaker
from threading import Thread

class BuildGrid(LabelFrame):
	def __init__(self, parent, *args, **kwargs):
		super().__init__(parent, *args, **kwargs)
		def __browseFiles():
			filePath = filedialog.askopenfilename(initialdir = os.getcwd(), title = "Select a File", 
													filetypes = (("Video Files", "*.mp4*"),
													("all files", "*.*")))
			
			# Change label contents
			self.__input.delete(0,END)
			self.__input.insert(0,filePath)
			return
		
		def __browseDirectory():
			path = filedialog.askdirectory(initialdir = os.getcwd(), title = "Select a directory")
			self.__output.delete(0,END)
			self.__output.insert(0,path)
			return
		
		def __process():
			
			videoPath = self.__input.get()
			outPath = self.__output.get()
			
			if not os.path.exists(videoPath):
				print("video path not good")
			elif not os.path.exists(outPath):
				print("output path not good")
			else:
				gifMaker = GifMaker(videoPath, outPath)
				thread1 = Thread(target = __generate_gif, args = (gifMaker, ))
				thread1.start()
				
				while(gifMaker.get_progress() < 100):
					self.__progressBar['value'] = gifMaker.get_progress()
					self.update_idletasks()
				
				self.__progressBar['value'] = 100
				thread1.join()
				self.__output.get()
				gif = os.path.join(self.__output.get(), os.path.basename(self.__input.get()))
				tkinter.messagebox.showinfo("Job Done",  "Gif is generated : {}".format(gif))
				
				# reset ui
				self.__input.delete(0,END)
				self.__output.delete(0,END)
				self.__progressBar['value'] = 0

			return
		
		def __generate_gif(arg):
			arg.generateGif()
		
		# input path
		self.__input_label = Label(self, text="input path")
		self.__input_label.grid(row=0, column=0, sticky="e")
		self.__input = Entry(self)
		self.__input.grid(row=0, column=1, sticky="ew")
		self.__button_filepath = Button(self, text = "Browse Files", command = __browseFiles)
		self.__button_filepath.grid(column = 2, row = 0)
		
		# output path
		self.__output_label = Label(self, text="output path")
		self.__output_label.grid(row=0, column=3, sticky="e")
		self.__output = Entry(self)
		self.__output.grid(row=0, column=4, sticky="ew")
		self.__button_outpath = Button(self, text = "Browse Directory", command = __browseDirectory)
		self.__button_outpath.grid(column = 5, row = 0)
		
		
		self.__button_process = Button(self, text = "Generate gif", command = __process)
		self.__button_process.place(rely=1.0, relx=1.0, x=0, y=0, anchor=SE)
		
		self.__progressBar = Progressbar(self, orient = HORIZONTAL, length = 600, mode = 'determinate')
		self.__progressBar['value'] = 0
		self.__progressBar.place(rely=1.0, relx=0.0, x=0, y=0, anchor=SW)
		
		self.grid_columnconfigure((1,3,5), weight=1)
		
	
		
		
		
		
	
	
