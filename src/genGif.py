import sys, os
import argparse
PROJECT_ROOT = os.path.abspath(os.path.join(
                  os.path.dirname(__file__), 
                  os.pardir)
)
sys.path.append(os.path.join(PROJECT_ROOT, "include"))

from gifMaker import GifMaker

from threading import Thread
from time import sleep

################# Main ####################

parser = argparse.ArgumentParser(description='Video to giff')
parser.add_argument('VideoFilePath', type=str, help='video file path')
parser.add_argument('outputPath', type=str, help='gif output directory')
parser.add_argument('-mfs','--maxOutputFileSize', type=int, help='maximum gif file size (in Mega Bytes)')
parser.add_argument('-r','--resolution', type=int, nargs=2, help='gif resolution(y,x), ex: -r 640 360', default=[640, 360])
args = parser.parse_args()


videoPath = os.path.abspath(args.VideoFilePath)
outPath = os.path.abspath(args.outputPath)
maxOutputFileSize = args.maxOutputFileSize
resolution_WH = args.resolution

if not os.path.isfile(videoPath): # TODO check video format
	print("Path to video file is not a file")
	sys.exit(1)

def generate_gif(arg):
	arg.generateGif()
	

def show_progress(pourcent):
	sys.stdout.write('\x1B[2K') # Erase entire current line
	sys.stdout.write('\x1B[0E') # Move to the beginning of the current line
	progress = "Progress: ["
	for i in range(0, 40):
		if i < 40 * pourcent/100:
			progress += '='
		else:
			progress += ' '
	progress += "] " + str(pourcent) + "%"
	sys.stdout.write(progress)
	sys.stdout.flush()
	sleep(0.1)

def progress(arg):
	while(arg.get_progress() < 100):
		progress = arg.get_progress()
		show_progress(progress)
	show_progress(100)
	print("\ndone")

gifMaker = GifMaker(videoPath, outPath, resolution_WH[0], resolution_WH[1])
thread1 = Thread(target = generate_gif, args = (gifMaker, ))
thread2 = Thread(target = progress, args = (gifMaker, ))
thread1.start()
thread2.start()
thread1.join()
thread2.join()
