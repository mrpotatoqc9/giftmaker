import sys, os

PROJECT_ROOT = os.path.abspath(os.path.join(
                  os.path.dirname(__file__), 
                  os.pardir)
)
sys.path.append(os.path.join(PROJECT_ROOT, "include"))

from rq import get_current_job

from gifMaker import GifMaker


def gen_gif(videoPath, outputPath):
	job = get_current_job()
	print(job.id)
	gifMaker = GifMaker(videoPath, outputPath)
	gifMaker.generateGif()
	
	return {
        	"job_id": job.id
			}


def report_success(job, connection, result, *args, **kwargs):
	print("success")
	#print(job, result, args)

def report_failure(job, connection, type, value, traceback):
	print("error gif processing")
