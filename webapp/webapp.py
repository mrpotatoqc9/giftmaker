from flask import Flask, request, jsonify, send_file
from flask_wtf import FlaskForm
from wtforms import FileField, SubmitField
from werkzeug.utils import secure_filename
import os, sys
from wtforms.validators import InputRequired
from flask_socketio import SocketIO, send, emit
import time, base64
import redis
from rq.job import Job
from rq import Queue

from worker import gen_gif, report_failure, report_success

"""
redis connexion
"""

redis_conn = redis.Redis(
    host=os.getenv("REDIS_HOST", "127.0.0.1"),
    port=os.getenv("REDIS_PORT", "6379"),
    password=os.getenv("REDIS_PASSWORD", ""),
)

redis_queue = Queue(connection=redis_conn)



"""
Flask config
"""
app = Flask(__name__)
app.config['workDir'] = 'public/workingDir'
app.config['SECRET_KEY'] = "SECRETVERYMUCH"


idCounter = 0 # TODO check workingDir or clean workingDir

@app.route("/")


@app.route("/process", methods=["POST"])
def process():
	global idCounter
	if request.method == 'POST':
		try:
			videoFile = request.files['file']
			ID = request.form['id']
			videoPath = os.path.join(app.config['workDir'], ID, videoFile.filename)
			outpuPath = os.path.join(app.config['workDir'], ID)
			job = redis_queue.enqueue(gen_gif, args=(videoPath, outpuPath,), job_id=ID, on_success=report_success, on_failure=report_failure)		
			
		except Exception as e:
			print(str(e))
			return(str(e))
		
		return jsonify({"job_id": job.id})
	else :
		return "error"


@app.route('/upload', methods=["POST"])
def upload():
	global idCounter
	idCounter += 1
	if request.method == 'POST':
		try:
			videoFile = request.files['file']
		except Exception as e:
			print(str(e))
			
		try:
			os.mkdir(os.path.join(app.config['workDir'], str(idCounter)))
		
		except Exception as e:
			print(str(e))
			
		try:
			videoFile.save(os.path.join(os.path.abspath(os.path.dirname(__file__)),app.config['workDir'], str(idCounter), secure_filename(videoFile.filename)))
		
		except Exception as e:
			print(str(e))
		
		return str(idCounter)
	else:
		return "Error"

@app.route("/checkStatus", methods=['GET'])
def checkStatus():
	job_key = request.args["id"]
	job = Job.fetch(job_key, connection=redis_conn)
	
	while not job.is_finished:
		job = Job.fetch(job_key, connection=redis_conn)
		time.sleep(0.01)
	
	return "ok"

@app.route("/getResults", methods=['GET'])
def get_results():
	print(request)
	job_key = request.args["id"]
	print(job_key)
	#filename = '/home/pat/projets/giftmaker/webapp/public/workingDir/1/openmp.gif'
	
	path = os.path.join(os.path.abspath(os.path.dirname(__file__)),app.config['workDir'], str(job_key))
	for f in os.listdir(path):
		print(f)
		if f.endswith(".gif"):
			img = f
	
	filepath = os.path.join(path, img)
	
	encondedZip = ""

	with open(filepath, "rb") as f:
		encodedZip = base64.b64encode(f.read())
	
	return encodedZip.decode()

"""
Running app
"""
if __name__ == '__main__':
	app.run(debug=True)
