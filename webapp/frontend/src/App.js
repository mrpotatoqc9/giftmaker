import React, {useState} from "react";
import "./App.css";
import logo from "./splash-no-Background.png"
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Download } from "./components/downloadResult"
import { FileUPloader } from "./components/fileUploader"
import {Spinner} from "./components/loadingWheel"


function App() {
	
	const [file, setFile] = useState(null);
    const [processing, setProcessing] = useState(false);
    const [generated, setGenerated] = useState(false);
	const [jobID, setjobID] = useState(false);
	
	
	return(
		<header className="App-header">
				<img src={logo} alt="logo" />
				<div>

					<FileUPloader videoFile={file} setFile={setFile} setProcessing={setProcessing} setGenerated={setGenerated} setjobID={setjobID}/>
					{processing && <Spinner/> }
					{generated && <Download jobID={jobID} />}
					
				</div>
				<ToastContainer/>
		</header>

					
	);
}

export default App;
