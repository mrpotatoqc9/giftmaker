import axios from 'axios';
import {useState, useEffect} from "react";

export function Download ({jobID}) {
	
	const [img, setImg] = useState(null);
	
	useEffect(() => {
        //console.log("loaded");
        getResults();
     });
	
    function getResults() {       
		//console.log("ok")
		const params = new URLSearchParams([['id', jobID]]);
        axios.get('/getResults', {params}, {responseType: "arraybuffer"})
            .then((response) => {
			  setImg(response.data)
            })
            .catch((e) => {
            	console.log(e)
            })
     };
     
     function download(){
     	const params = new URLSearchParams([['id', jobID]]);
        axios.get('/getResults', {params}, {responseType: "blob"})
            .then((response) => {
			  //const href = URL.createObjectURL(response.data);

			// create "a" HTML element with href to file & click
			const link = document.createElement('a');
			link.href = "data:image/png;base64," + response.data;
			link.setAttribute('download', 'yourgif.gif'); //or any other extension
			document.body.appendChild(link);
			link.click();

			// clean up "a" element & remove ObjectURL
			document.body.removeChild(link);
            })
            .catch((e) => {
            	console.log(e)
            })
     };


    return(
     <div>
     	<div>
     		<img src={`data:image/png;charset=utf-8;base64,${img}`} />
	 	</div>
	 	<div>
	 		<button onClick={download}> Download </button>
	 	</div>
	 </div>
	)
};

