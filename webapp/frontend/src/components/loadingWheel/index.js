import React from 'react';
import ClipLoader from 'react-spinners/ClipLoader';

export function Spinner() {
  return (
    <div style={{ width: '100px', margin: 'auto', display: 'block' }}>
      <ClipLoader color="#52bfd9" size={35}/>
    </div>
  );
};
