import axios from 'axios';
import {toast} from 'react-toastify';
import './style.css';

export function FileUPloader ({videoFile, setFile, setProcessing, setjobID, setGenerated}) {
	
	function send_to_processing_q(id) {
		
		const data = new FormData();
        data.append('file', videoFile)
        data.append('id', id)
        axios.post('/process', data)
            .then((response) => {
                setProcessing(true)
                checkStatus(id)
            })
            .catch((e) => {
            	console.log(e)
                toast.error('Job not created')
                setProcessing(false)
            })
	setFile("")
	document.getElementById("input").value = "";
    };
    
    function checkStatus(id) {

		const params = new URLSearchParams([['id', id]]);
        axios.get('/checkStatus', {params})
            .then((response) => {
                toast.success('Gif generated !');
                setProcessing(false)
                setGenerated(true)
            })
            .catch((e) => {
            	console.log(e)
                toast.error('Gif generation error')
                setProcessing(false)
            })
    };	
		

    const onInputChange = (e) => {
        setFile(e.target.files[0])
    };

    const onSubmit = (e) => {
        e.preventDefault();
		
		
        const data = new FormData();
        data.append('file', videoFile)
        axios.post('/upload', data)
            .then((response) => {
            	let job_id = response.data
                toast.success('Upload Success');
                setjobID(job_id)
                send_to_processing_q(job_id)
            })
            .catch((e) => {
                toast.error('Upload Error')
            })     
       
    };

    return(
		<form method="post" action="#" id="#" onSubmit={onSubmit}>
			<div className="form-group files">
			  <label>Upload Your File </label>
			  <input id ="input" type="file" onChange={onInputChange} className="form-control" multiple="" 
			  accept=".avi, .mpg, .mp2, .mpeg, .mpe, .mpv, .mp4, .webm"/>
			</div>
			<button> Submit </button>
		</form>
	)
	
	
};
