import unittest
import sys, os

PROJECT_ROOT = os.path.abspath(os.path.join(
                  os.path.dirname(__file__), 
                  os.pardir)
)
sys.path.append(os.path.join(PROJECT_ROOT, "include"))

from gifMaker import GifMaker

class TestGifMaker(unittest.TestCase):
	def test_gen_gif(self):
		gifMaker = GifMaker("data/openmp.mp4", os.getcwd())
		self.assertIsInstance(gifMaker, GifMaker)
		
		gifMaker.set_frame_2_keep_per_second(10)
		self.assertTrue(gifMaker.frame_2_keep_per_second_is_set())
		
		gifMaker.set_gif_duration(10)
		self.assertFalse(gifMaker.gif_duration_is_set())
		
		del gifMaker
		gifMaker = GifMaker("data/openmp.mp4", os.getcwd())
		gifMaker.set_gif_duration(10)
		self.assertTrue(gifMaker.gif_duration_is_set)
		
		gifMaker.set_frame_2_keep_per_second(10)
		self.assertFalse(gifMaker.frame_2_keep_per_second_is_set())
		
		del gifMaker
		gifMaker = GifMaker("data/openmp.mp4", os.getcwd())
		gifMaker.generateGif()
		self.assertTrue(os.path.exists(os.path.join(os.getcwd(),"openmp.gif")))
		

if __name__ == '__main__':
    unittest.main()
