# Video2Giff
![](data/splash-no-Background.png)

# Donate
You like GifMaker ? Let me know [here](https://www.buymeacoffee.com/patcmorneaX)

<br/>

## Windows instructions
[Download](https://patcmpro.ca/fr/documentation/gifMaker) GifMaker and start converting your video into gif


## Linux instructions

### install dependencies:
```
pip install -r requirements.txt
```

*optional* for GUI :
```
sudo apt-get install python3-tk
```

### Cli app usage example:
```
python src/genGif.py data/openmp.mp4 ./
```
### Help:
```
python src/genGif.py -h
```

</br>

## Web app

### 1- Install redis

```
sudo apt install redis-server
sudo nano /etc/redis/redis.conf
```
Inside the file, find the `supervised` directive. This directive allows you to declare an init system to manage Redis as a service, providing you with more control over its operation. The supervised directive is set to no by default. Since you are running Ubuntu, which uses the systemd init system, change this to `systemd`.
It will look like :
`supervised systemd`

then restart
```
sudo systemctl restart redis.service
sudo systemctl status redis
redis-cli
```

### 2- Start flask server
```
python webapp/webapp.py
```
### 3- Start worker
```
rq worker high default low
```

### 4- Start react frontend
```
cd webapp/frontend
npm start
``` 


## Gui app
```
python ui/gui_main.py
```

## Unit test
```
python test/tests.py
```

## Generate executable
needs to be done in Windows
```
pyinstaller.exe --icon data\icon.ico -p include ui\gui_main.py --onefile --windowed -n GifMaker
```


## TODO :
- Docker container
- Test web app
- Documentation

